![Docker Logo](/assets/images/docker.png "Docker") ![Kubernetes Logo](/assets/images/kubernetes.png "Kubernetes") 

# Willkommen beim Docker / Kubernetes Workshop

Heute möchten wir Ihnen zeigen, wie Docker und Kubernetes im Hand-on funktionieren.
Folgende Themen werden voraussichtlich behandelt.

#### Docker

* Wie startet/stoppt man einen Docker Container?
* Wie erstellt man ein eigenes Docker Image?
* Wie verbindet man Docker Container?
* Wie kann man Daten persistent speichern und im Docker Container verwenden?
* Wie konfiguriert man Docker Container?
* Wie verwendet man eine Docker Registry?
* ...

#### Kubernetes

* Wie deployed man einen Docker Container in Kubernetes?
* Wie macht man einen Docker Container in Kubernetes erreichbar?
* Wie verbindet man Docker Container in Kubernetes?
* Wie funktioniert Persistenz in Kubernetes?
* Wie kann man Applikationen mit Kubernetes high available betreiben?
* Wie kann man Applikationen mit Kubernetes selbstheilend betreiben?
* ...

Q & A

* Fragerunde

---

## Setup

Um diesen Docker / Kubernetes Workshop durchzuführen, werden keine lokale Docker oder Kubernetes Installationen benötigt / verwendet. Um den Supportaufwand gering zu halten, wird jeder Teilnehmer einen eigenen vorkonfigurierten Client in AWS erhalten, der alles Nötige enthält.

Am Anfang des Workshops wird ein USB-Stick die Runde machen, auf dem ein SSH Private Key gespeichert ist. **Bitte kopieren Sie diesen Key auf Ihren lokalen Rechner.** Falls Sie noch keinen SSH Client auf Ihrem Rechner installiert haben, ist auf dem USB-Stick auch ein Portable SSH-Client.

Ihre Client IP und user bekommen Sie von den Vortragenden ausgehändigt.

Um eine SSH-Verbindung zu Ihrem Client aufzubauen, folgende Sie bitte diesen Schritten:

* Starten Sie Ihren SSH Client (entweder MobaXTerm, Putty, ...)
* Erstellen Sie eine neue Verbindung
  * Für MobaXTerm 
      * Klicken Sie oben links auf "Session"
      * Klicken Sie auf SSH
      * Tragen Sie die Ihnen zur Verfügung gestellte IP Adresse in das Feld "Remote Host" ein
      * Tragen Sie in das Feld "Specify username" den Text "ec2-user" ein
      * Klicken Sie auf den Tab "Advanced SSH Settings"
      * Klicken Sie auf die Checkbox vor dem Feld "Use private key"
      * Klicken Sie auf das Dateisymbol im Textfeld und suchen Sie nach dem Ihnen zur Verfügung gestellten "docker-workshop.ppk" File und wählen dieses aus.
      * Klicken Sie auf "OK" und Doppel Klicken Sie auf die neu erstellt Session
  * Für Putty
        * Tragen Sie die Ihnen zur Verfügung gestellte IP Adresse in das Feld "Host Name" ein
        * Klicken Sie links auf den Eintrag "Connection->Data"
        * Tragen Sie in das Feld "Auto-login username" den Text "ec2-user" ein
        * Klicken Sie links auf den Eintrag "Connection->SSH->Auth"
        * Klicken Sie auf den Button "Browse" und suchen Sie nach dem Ihnen zur Verfügung gestellten "docker-workshop.ppk" File und wählen dieses aus.

---

## Agenda

* [Review](#review)
  * [Docker Begriffe](#docker-begriffe)
  * [Kubernetes Begriffe](#kubernetes-begriffe)
* [Setup](#setup)
* [Docker Details](#docker-details)
  * [Docker CLI](#docker-cli)
  * [Dockerfile](#dockerfile)
  * [Docker Registry](#docker-registry)
  * [Docker Aufgaben](#docker-aufgaben)
* [Kubernetes Details](#kubernetes-details)
  * [Kubernetes CLI](#kubernetes-cli)
     * [Kubernetes Ressourcen](#kubernetes-ressourcen)
     * [Kubernetes Befehle](#kubernetes-befehle)
  * [Kubernetes Deployment Templates](#kubernetes-deployment-templates)
  * [Kubernetes Aufgaben](#kubernetes-aufgaben)

---

## Review

In den folgenden Kapiteln werden nochmal auf die Schnelle, alle wichtigen Begrifflichkeiten erklärt, die heute wichtig sind.

### Docker Begriffe

Container<sup>[1][Docker Container]</sup>
: Ein Docker Container ist ein leichtgewichtiges Software Paket, das alles Nötige für die eigene Ausführung enthält.

Image
: Ein Docker Image ist die Basis für jeden Container. Aus einem Image können beliebig viele Container erzeugt werden. Ein laufender Container kann keine Änderungen an seinem Image Vornehmen.

Dockerfile<sup>[2][Dockerfile]</sup>
: Ein Dockerfile ist eine Anleitung zum Erstellen eines Images.

Registry
: Eine Docker Registry ist ein zentraler Ort, an dem Docker Images gespeichert werden können. Siehe [Docker Registry](#docker-registry).

Tags
: Ein Docker Tag ist ein Name für eine bestimmte Version eines Docker Images. `latest` ist der Standard Tag und bezeichnet per Konvention immer das aktuellste Image.

Volume
: Ein Docker Volume ist ein Mittel um Daten über die Lebensdauer eines Containers hinaus persistent zu machen.

Network
: Ein Docker Network ist eine Netzwerkverbindung zwischen zwei oder mehr Containern

Docker CLI<sup>[3][Docker CLI]</sup>
: Die Docker CLI ist die Textbasierte Befehlsschnittstelle für Docker  

Docker Hub<sup>[4][Docker Hub]</sup>
: Das Docker Hub ist die von Docker Inc. selbst gehostete Standard Registry 

[Dockerfile]: https://docs.docker.com/engine/reference/builder/ "Dockerfile Reference"
[Docker CLI]: https://docs.docker.com/engine/reference/commandline/cli/ "Docker CLI Reference"
[Docker Container]: https://www.docker.com/what-container "Docker Container Reference"
[Docker Hub]: https://hub.docker.com/ "Docker Hub"

### Kubernetes Begriffe

Pod<sup>[1][Kubernetes Pod]</sup>
: Ein Pod ist die Kubernetes Abstraktion für einen oder mehrere Container. Sie sind kurzlebig.

Service<sup>[2][Kubernetes Service]</sup>
: Ein Service ist ein LoadBalancer der für alle Pods einer definierten Gruppe eine stabile Adresse bereitstellt.

Deployment<sup>[3][Kubernetes Deployment]</sup>
: Ein Deployment ist ein Objekt zur Verwaltung von Versionen von Pods. Es erzeugt ReplicaSets.

ReplicaSet<sup>[4][Kubernetes ReplicaSet]</sup>
: Ein ReplicaSet ist ein Objekt zur Verwaltung der Anzahl der Instanzen eines definierten Pod Templates.

StatefulSet<sup>[5][Kubernetes StatefulSet]</sup>
: Ein StatefulSet ist ein Objekt zur Verwaltung von Pods einer statusbehafteten Anwendung. Zum Beispiel Datenbanken.

Ingress<sup>[6][Kubernetes Ingress]</sup>
: Ein Ingress definiert, für welche Kombination aus Hostname und Kontextpfad, welcher Service verantwortlich ist. 

Peristent Volume (Claim)<sup>[7][Kubernetes Peristent Volume]</sup>
: Ein Persistent Volume ist die Kubernetes Abstraktion eines persistenten Speicherplatzes. Zum Beispiel eine NFS Share. Es gibt Adapter für viele verschiedenen Technologien. Persistent Volumes können entweder automatisch provisioniert werden oder von einem Cluster Administrator von Hand angelegt werden.

    Ein Persistent Volume Claim ist die Anmeldung eines Bedarfes an persistentem Speicher. Wenn ein Persistent Volume diesen Bedarf erfüllen kann, wird der Claim und das Volume permanent verbunden.

Config Map / Secret<sup>[8][Kubernetes ConfigMap]</sup>,<sup>[9][Kubernetes Secret]</sup>
: Eine ConfigMap ist ein Speicher für Konfigurationswerte und -dateien. Diese Werte können einem Container übergeben werden. Damit muss für eine Konfigurationsänderung eines Containers nicht das dazugehörige Image neu erstellt werden. 
  
    Ein Secret ist ein Speicher für sensible Daten und Dateien, welche wie bei einer ConfigMap einem Container übergeben werden können.

[Kubernetes Pod]: https://kubernetes.io/docs/concepts/workloads/pods/pod/ "Kubernetes Pods"
[Kubernetes Service]: https://kubernetes.io/docs/concepts/services-networking/service/ "Kubernetes Service"
[Kubernetes Deployment]: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/ "Kubernetes Deployment"
[Kubernetes ReplicaSet]: https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/ "Kubernetes ReplicaSet"
[Kubernetes StatefulSet]: https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/ "Kubernetes StatefulSet"
[Kubernetes Ingress]: https://kubernetes.io/docs/concepts/services-networking/ingress/ "Kubernetes Ingress"
[Kubernetes Peristent Volume]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/ "Kubernetes Peristent Volume (Claim)"
[Kubernetes ConfigMap]: https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/ "Kubernetes ConfigMap"
[Kubernetes Secret]: https://kubernetes.io/docs/concepts/configuration/secret/ "Kubernetes Secret"

[Zur Agenda](#agenda)

---

## Docker Details

### Docker CLI

Alles was lokal mit Docker zu tun hat, wird über die Docker CLI gesteuert. Der Befehl dazu heißt schlicht _docker_. Im Folgenden werden kurz die wichtigsten Befehle beschrieben.

docker run [options] {image}[:tag] \[command]
: Mit _docker run_ kann man Container starten. Hierfür gibt es viele Parameter, doch die wichtigsten sind die Folgenden:
* **-p:** Mappen eines externen Ports des Hosts auf einen internen Port des Containers (z.B. -p 8080:80 -> externer Port:Interner Port)
* **-v:** Mappen eines externen Dateisystem Pfades des Hosts bzw. ein von Docker erstelltes Volume auf einen internen Pfad im Dateisystem des Containers (z.B. -v /home/user/nginx/html:/usr/share/nginx/html -> externer Pfad bzw. volume:interner Pfad)
* **-d:** Starten des Containers im Hintergrund
* **-it:** Pflichtparameter, wenn kein Hintergrundprozess gestartet wird, sondern man sich direkt mit einem Command in den Container verbinden möchte (z.B. mit dem Command _bash_ oder _sh_).
* **- -name:** Vergabe eines selbst definierten Namens für den Container. Über den Namen kann der Container in weiteren Befehlen direkt angesprochen werden.
* **- -rm:** Nützlich, wenn kein Hintergrundprozess gestartet wird, sondern man sich direkt in den Container verbinden möchte. Damit wird der Container direkt nach Verlassen des Containers gelöscht.
* **-e:** Setzen einer Umgebungsvariablen (z.B. -e USE_TLS=true)
* **- -network**: Setzen des Netzwerks mit dem sich der Container verbinden soll.

docker ps
: Mit _docker ps_ werden alle aktuell gestarteten Container angezeigt. Wenn zusätzlich die gestoppten Container angezeigt werden sollen, kann der Parameter _-a_ angegeben werden.

docker start/stop {container} [{container}...]
: Mit _docker start/stop_ können gestoppte Container gestartet bzw. gestartete Container gestoppt werden. Als Identifikation eines Conatiner kann entweder die Container ID oder der Name des Containers verwendet werden.

docker rm {container} [{container}...]
: Mit _docker rm_ können Container gelöscht werden, wenn sie nicht mehr gebraucht werden. Gelöscht werden können im Normalfall nur gestoppte Container. Es gibt die Möglichkeit auch laufende Container mit dem Parameter _-f_ zu löschen.

docker rmi {image} [{image}...]
: Mit _docker rmi_ können Images gelöscht werden.

docker build -t {image}[:tag] .
: Mit _docker build_ werden neue Images erstellt, basierend auf einem Dockerfile. Das Dockerfile muss in dem Ordner liegen, in dem der Befehl ausgeführt wird. Vorsicht: Bitte den Punkt am Ende nicht vergessen!

<a id="docker-tag" />docker tag {image}[:tag] {newImage}[:newTag]
: Mit _docker tag_ kann ein Image unter einem anderen Namen bekannt gemacht werden. Dabei ändert sich lediglich der Name des Images. Es wird kein neues Image erstellt! Dieser Befehl wird meistens dann verwendet, wenn ein Image in eine Custom Docker Registry gepusht werden soll.

docker pull {image}[:tag]
: Herunterladen eines Images aus einer [Docker Registry](#docker-registry).

<a id="docker-push" />docker push {image}[:tag}
: Hochladen eines lokalen Images in eine [Docker Registry](#docker-registry).

<a id="docker-login" />docker login
: Login bei einer [Docker Registry](#docker-registry).

docker volume {command}
: Mit _docker volume_ können persistente Volumes erstellt und verwaltet werden. Wichtige commands sind:
* **create [name]**: Erstellen eines Volumes (optional mit Namen). Es gibt hier keine Möglichkeit die Größe eines Volumes anzugeben!
* **ls**: Anzeigen aller Volumes
* **rm**: Löschen eines Volumes

docker network {command}
: Mit _docker network_ können interne Netzwerke erstellt und verwaltet werden. Wichtige commands sind:
* **create {name}**: Erstellen eines benannten Netzwerks
* **ls**: Anzeigen aller Netzwerke
* **rm**: Löschen eines Netzwerks
* **connect {network} {container}**: Nachträgliches Hinzufügen eines Containers zu einem Netzwerk
* **disconnect {network} {container}**: Nachträgliches Entfernen eines Containers aus einem Netzwerk

docker inspect {container}|{image}
: Mit _docker inspect_ kann man alle Meta Informationen zu einem Image bzw. Container anzeigen

docker [system|container|image|network|volume] prune
: Mit _docker ... prune_ werden alle nicht verwendeten Objekte gelöscht (z.B. alle nicht gestarteten Container, alle nicht verwendeten Images, ...). Wenn _system_ verwendet wird, werden alle nicht verwendeten Objekte gelöscht!

[Zur Agenda](#agenda)

### Dockerfile

Das [Dockerfile][Dockerfile] (die Datei muss auch so heißen) ist eine einfache Textdatei, das zeilenweise angibt, wie ein Docker Image aufgebaut ist. Dabei gibt es verschiedene Möglichkeiten das Basis Image zu verändern. Für das Erstellen eines neuen Images gibt es auch [Best Practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices) direkt von Docker.

* **FROM:** Definition des Basis Images **Required** (z.B. FROM ubuntu:xenial)
* **MAINTAINER:** Einfache Beschreibung, wer das Docker Image erstellt hat. Meistens in Form eines Namens und einer Email Adresse.
* **ENV:** Setzen von statischen Umgebungsvariablen. Kann z.B. genutzt werden, um die im Container genutzte Software Version zu setzen (z.B. NGINX_VERSION=1.13.1)
* **COPY:** Kopieren von Dateien des buildenden Systems in das neue Docker Image (z.B. COPY index.html /usr/share/nginx/html/index.html)
* **CMD:** Setzen des Commands, welcher beim Starten des Containers ausgeführt werden soll, wenn kein expliziter Command beim starten angegeben wurde. (z.B. CMD java -jar app.jar)
* **ENTRYPOINT:** Setzen des Commands, der immer beim Starten ausgeführt werden soll, egal ob und was unter CMD gesetzt wird. Wenn dies benötigt wird, wird normalerweise hier ein Skript aufgerufen, das den Container initialisiert und danach dem CMD Command ausführt. (z.B. ENTRYPOINT /docker-entrypoint.sh)
* **EXPOSE:** Angabe aller Ports, die intern vom Container exposed werden. Diese Angabe ist rein optional! Auch wenn der Container einen Port exposed, muss dies im Dockerfile nicht angegeben werden! (z.B. EXPOSE 80)
* **VOLUME:** Angabe aller Verzeichnisse, die intern als potenziell Persistente Ordner verwendet werden. D.h. hier werden evtl. Konfigurationsdateien abgelegt oder die Daten einer Datenbank. Diese Angabe ist rein optional! (z.B. VOLUME /etc/nginx/config.d)
* **RUN:** Hiermit können im Image Befehle ausgeführt werden. Konkret funktioniert es so, dass der Container kurzzeitig gestartet wird, der Command ausgeführt wird und danach wieder gestoppt wird. Das Ergebnis ist die Grundlage für den nächsten Befehl im Dockerfile. (z.B. RUN apt-get update && apt-get install telnet -y && rm -rf /var/lib/apt/lists/*)
* **USER:** Setzen des ausführenden Unix Users im Container
* **WORKDIR:** Setzen des Verzeichnisses, das beim Starten des Containers ausgewählt wird
* **LABEL:** Setzen von Metadata Key-Value-Pairs

[Zur Agenda](#agenda)

### Docker Registry

Eine Docker Registry ist ein zentraler Ort, an dem Docker Images gespeichert werden können. Die bekannteste Registry ist das [Docker Hub][Docker Hub]. Docker ist standardmäßig so eingerichtet, dass es automatisch von dort die Images bezieht. Wenn nun Images aus einer eigenen Docker Registry bezogen werden sollen, aus Performance Gründen (Docker Images können sehr groß sein) oder weil man seine Docker Images privat halten möchte, muss das Docker Image nach Folgendem Muster getaggt werden.

```
[registry-host[:registry-port]/][username/]image[:tag]
```

Es ist nicht notwendig ein Protokoll anzugeben, da Docker immer von HTTPS ausgeht. Für den Fall, dass die Registry nur via HTTP erreichbar ist oder kein valides Zertifikat besitzt, kann eine Registry in den Docker Settings auch als "insecure-registry" eingestellt werden. Dies sollte aber nur in Ausnahmefällen genutzt werden, z.B. bei Kunden, die eine eigene Certificate Chain bereitstellen.

Ist das Docker Image entsprechend getaggt, kann es in die [Registry gepusht][Docker Push] werden. Evtl. ist zuvor ein [Login][Docker Login] nötig, wenn die Registry Passwort geschützt ist.

[Docker Login]: #docker-login "Docker Login"
[Docker Push]: #docker-push "Docker Push""

[Zur Agenda](#agenda)

### Docker Aufgaben

1. Starten Sie den Hello-World Container (vom Docker Hub) mit dem _docker run_ Befehl
``` 
docker run --rm hello-world
```

2. Starten Sie einen Webserver (z.B. nginx) auf dem Port 80. Rufen Sie danach die Website auf (http://{CLIENT-IP}).
```
docker run --rm -p 80:80 reg.k.vision-spirit21.com/nginx
```

3. Erstellen Sie ihr erstes eigenes Docker Image. Sie werden in den nächsten Schritten sehen, was Docker Layers sind und wie das Basis Image Konzept funktioniert.

    Legen Sie dazu eine Datei mit dem Namen "Dockerfile" in dem Ordner "Aufgabe3" an und kopieren den folgenden Inhalt hinein.
    
    ```
    mkdir Aufgabe3
    nano Dockerfile
    ```
<details><summary>Click to open</summary>
```
FROM debian:stretch-slim
LABEL maintainer="NGINX Docker Maintainers <docker-maint@nginx.com>"
ENV NGINX_VERSION 1.13.9-1~stretch
ENV NJS_VERSION   1.13.9.0.1.15-1~stretch
RUN set -x \
        && apt-get update \
        && apt-get install --no-install-recommends --no-install-suggests -y gnupg1 apt-transport-https ca-certificates \
        && \
        NGINX_GPGKEY=573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62; \
        found=''; \
        for server in \
            ha.pool.sks-keyservers.net \
            hkp://keyserver.ubuntu.com:80 \
            hkp://p80.pool.sks-keyservers.net:80 \
            pgp.mit.edu \
        ; do \
            echo "Fetching GPG key $NGINX_GPGKEY from $server"; \
            apt-key adv --keyserver "$server" --keyserver-options timeout=10 --recv-keys "$NGINX_GPGKEY" && found=yes && break; \
        done; \
        test -z "$found" && echo >&2 "error: failed to fetch GPG key $NGINX_GPGKEY" && exit 1; \
        apt-get remove --purge --auto-remove -y gnupg1 && rm -rf /var/lib/apt/lists/* \
        && dpkgArch="$(dpkg --print-architecture)" \
        && nginxPackages=" \
            nginx=${NGINX_VERSION} \
            nginx-module-xslt=${NGINX_VERSION} \
            nginx-module-geoip=${NGINX_VERSION} \
            nginx-module-image-filter=${NGINX_VERSION} \
            nginx-module-njs=${NJS_VERSION} \
        " \
        && case "$dpkgArch" in \
            amd64|i386) \
# arches officialy built by upstream
                echo "deb https://nginx.org/packages/mainline/debian/ stretch nginx" >> /etc/apt/sources.list.d/nginx.list \
                && apt-get update \
                ;; \
            *) \
# we're on an architecture upstream doesn't officially build for
# let's build binaries from the published source packages
                echo "deb-src https://nginx.org/packages/mainline/debian/ stretch nginx" >> /etc/apt/sources.list.d/nginx.list \
                \
# new directory for storing sources and .deb files
                && tempDir="$(mktemp -d)" \
                && chmod 777 "$tempDir" \
# (777 to ensure APT's "_apt" user can access it too)
                \
# save list of currently-installed packages so build dependencies can be cleanly removed later
                && savedAptMark="$(apt-mark showmanual)" \
                \
# build .deb files from upstream's source packages (which are verified by apt-get)
                && apt-get update \
                && apt-get build-dep -y $nginxPackages \
                && ( \
                    cd "$tempDir" \
                    && DEB_BUILD_OPTIONS="nocheck parallel=$(nproc)" \
                    apt-get source --compile $nginxPackages \
                ) \
# we don't remove APT lists here because they get re-downloaded and removed later
                \
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
# (which is done after we install the built packages so we don't have to redownload any overlapping dependencies)
                && apt-mark showmanual | xargs apt-mark auto > /dev/null \
                && { [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; } \
                \
# create a temporary local APT repo to install from (so that dependency resolution can be handled by APT, as it should be)
                && ls -lAFh "$tempDir" \
                && ( cd "$tempDir" && dpkg-scanpackages . > Packages ) \
                && grep '^Package: ' "$tempDir/Packages" \
                && echo "deb [ trusted=yes ] file://$tempDir ./" > /etc/apt/sources.list.d/temp.list \
# work around the following APT issue by using "Acquire::GzipIndexes=false" (overriding "/etc/apt/apt.conf.d/docker-gzip-indexes")
#   Could not open file /var/lib/apt/lists/partial/_tmp_tmp.ODWljpQfkE_._Packages - open (13: Permission denied)
#   ...
#   E: Failed to fetch store:/var/lib/apt/lists/partial/_tmp_tmp.ODWljpQfkE_._Packages  Could not open file /var/lib/apt/lists/partial/_tmp_tmp.ODWljpQfkE_._Packages - open (13: Permission denied)
                && apt-get -o Acquire::GzipIndexes=false update \
                ;; \
        esac \
        \
        && apt-get install --no-install-recommends --no-install-suggests -y \
                            $nginxPackages \
                            gettext-base \
        && apt-get remove --purge --auto-remove -y apt-transport-https ca-certificates && rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/nginx.list \
        \
# if we have leftovers from building, let's purge them (including extra, unnecessary build deps)
        && if [ -n "$tempDir" ]; then \
            apt-get purge -y --auto-remove \
            && rm -rf "$tempDir" /etc/apt/sources.list.d/temp.list; \
        fi    
# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
        && ln -sf /dev/stderr /var/log/nginx/error.log
EXPOSE 80
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]
COPY index.html /usr/share/nginx/html/index.html
```
</details>
    
    Jetzt kopieren Sie bitte folgenden Content in die Datei _index.html_ im selben Ordner "Aufgabe3".
```html
<html>
    <body>
        Hallo Welt!
    </body>
</html>
```

    Nun können Sie das Docker Image bauen und starten
```
docker build ...
docker run ...
```
    
4. Wie Sie vielleicht bemerkt haben, ist das Erstellen des Docker Images nicht gerade schnell und das Dockerfile ist auch sehr aufwändig und lang. Um genau dies zu vermeiden, sind Docker Images in Layers unterteilt. Diese Layers können, wenn exakt gleich, zwischen Images geteilt werden. Außerdem können auf bereits bestehende Layers aufgebaut werden. zur Vereinfachung unseres Webserver Images werden wir nun die bereits bestehenden Layers von Nginx verwenden. Ändern Sie deshalb bitte das Dockerfile auf folgenden Content ab.
```
FROM reg.k.vision-spirit21.com/nginx
LABEL maintainer="Docker-Workshop <docker-workshop@spirit21.com>"
COPY index.html /usr/share/nginx/html/index.html
```

    Zum Ausführen des eigenen Images genügt es erneut die folgenden Befehle auszuführen.
```
docker build ...
docker run ...
```
    
    Beim erneuten Laden der Seite im Browser werden Sie keine Änderungen sehen. Nun versuchen Sie die _index.html_ Seite zu verändern und das Image noch einmal zu bauen und starten.

5. Im nächsten Schritt packen wir die _index.html_ nicht statisch in den Container, sondern mounten das File in den Container. 
```
docker run -d -v /home/ec2-user/Aufgabe3:/usr/share/nginx/html ...
```

    Ändern Sie die _index.html_ auf dem Client und rufen dann die Seite nochmal auf. Die Seite sollte sich dynamisch angepasst haben.

#### Beispiel Applikation

Nun sollten die wichtigsten Grundlagen bekannt sein. Daher beginnen wir nun unsere Beispiel Applikation aufzubauen.

1. Als Datenbank wird die NoSQL Datenbank MongoDB eingesetzt. Da hierfür im Bezug auf Docker keine gesonderte Konfiguration notwendig ist, laden wir ein bestehendes MongoDB Image herunter und [benennen das Image um](#docker-tag) und pushen es in die [Remote Registry](#docker-registry).
```
docker pull mongo
docker tag mongo reg.k.vision-spirit21.com/{USERNAME}/mongo
docker push reg.k.vision-spirit21.com/{USERNAME}/mongo
```

2. Um das Docker Image für Backend und Frontend zu erstellen, nutzen wir eine Möglichkeit von Docker, die es uns erlaubt einen komplexen multi-stage build durchzuführen. Was bedeutet _multi-stage build_? Dabei werden innerhalb eines Dockerfiles zwei build Prozesse nacheinander durchgeführt. Ergebnis ist trotzdem nur das eine Image, welches zuletzt im Build Prozess erstellt wurde. Damit kompilieren wir den Java Source Code bzw. die Angular TypeScript Applikation jeweils während des ersten Build Prozesses des Images und kopieren danach die Ergebnisse jeweils in ein ausführendes Image. 
```
// multi-stage build für das Backend
cd ~/docker-workshop/spring-boot-app-code
docker build -t {BACKEND-IMAGE} .
```
```
// multi-stage build für das Frontend
cd ~/docker-workshop/ui-app-code
docker build -t {FRONTEND-IMAGE} .
```

    Schließlich müssen die Images nur noch gepusht werden
```
docker tag {BACKEND-IMAGE} reg.k.vision-spirit21.com/{USERNAME}/api
docker push reg.k.vision-spirit21.com/{USERNAME}/api
docker tag {FRONTEND-IMAGE} reg.k.vision-spirit21.com/{USERNAME}/ui
docker push reg.k.vision-spirit21.com/{USERNAME}/ui
```

3. Nun, da die Images erstellt und gepusht sind, können wir die Kommunikation zwischen zwei Containern lokal testen. Dafür müssen wir zuerst ein Docker Netzwerk erstellen, damit die Container sich gegenseitig überhaupt sehen können. Danach werden wir die einzelnen Komponenten als Docker Container start
```
docker network create sample-app
docker run -d --net sample-app --name mongo reg.k.vision-spirit21.com/{USERNAME}/mongo mongod --replSet rs0 --bind_ip_all
docker run -d --net sample-app --name api -p 80:8080 -e SPRING_DATA_MONGODB_URI="mongodb://mongo:27017/heroes" -e KEYCLOAK_SSL_REQUIRED="none" reg.k.vision-spirit21.com/{USERNAME}/api
```

    Wenn Sie die Logfiles für das Backends aufrufen (`docker logs api`), dann sollten Sie keine Verbindungsprobleme sehen, was bedeutet, dass das Backend eine Verbindung erfolgreich erstellen konnte. Folgende Ausgaben sollten im Log zu sehen sein.
```
INFO 6 --- [           main] o.m.d.cluster                            : Cluster created with settings {hosts=[mongo:27017], mode=SINGLE, requiredClusterType=UNKNOWN, serverSelectionTimeout='30000 ms', maxWaitQueueSize=500}
INFO 6 --- ['}-mongo2:27017] o.m.d.connection                         : Opened connection [connectionId{localValue:1, serverValue:1}] to mongo:27017
INFO 6 --- ['}-mongo2:27017] o.m.d.cluster                            : Monitor thread successfully connected to server with description ServerDescription{address=mongo2:27017, type=REPLICA_SET_GHOST, state=CONNECTED, ok=true, version=ServerVersion{versionList=[3, 6, 2]}, minWireVersion=0, maxWireVersion=6, maxDocumentSize=16777216, roundTripTimeNanos=8477600, setName='null', canonicalAddress=null, hosts=[], passives=[], arbiters=[], primary='null', tagSet=TagSet{[]}, electionId=null, setVersion=null, lastWriteDate=null, lastUpdateTimeNanos=51933509032800}
```

Nun sind die für die Beispiel Applikation nötigen Docker Images in der Remote Registry vorhanden. Diese Images werden später für das Kubernetes Deployment verwendet. Im Folgenden werden nun Details zur Kubernetes CLI und den Ressourcen beschrieben. Wenn Sie direkt zu den Kubernetes Aufgaben springen möchten, folgend Sie [diesem Link](#kubernetes-aufgaben).

[Zur Agenda](#agenda)
	
---

## Kubernetes Details

[Kubernetes][Kubernetes] ist eine Orchestrierungsplattform für Docker, initial entwickelt von Google, jetzt Open Source. "Kubernetes ist eine Plattform für automatisiertes Deployment, Skalierbarkeit und Management von Container Applikationen."

[Kubernetes]: https://kubernetes.io/ "Kubernetes.io"

### Kubernetes CLI

#### Kubernetes Ressourcen

In Kubernetes gibt es viele verschiedene Ressourcen, die hier in Auszügen aufgelistet sind

Ressource             | Kurzform
----------------------|---------
Pods                  | po
Deployments           | deploy
StatefulSet           | sts
Services              | svc
PersistentVolume      | pv
PeristsnetVolumeClaim | pvc
Jobs                  | job
CronJobs              | cronjob
Ingress               | ing
Namespace             | ns
Nodes                 | node
ConfigMaps            | cm
Secrets               | secret

#### Kubernetes Befehle

kubectl get {ressource} \[id] [options]
: Mit _kubectl get_ können schnell wichtige Informationen zu allen laufenden Ressource auf der Plattform abgefragt werden. Es ist auch möglich die Information nur für eine konkrete Instanz (mit dessen ID) einer Ressource abzufragen. Dabei kann mit der Option _-o yaml_ die aktuelle Konfiguration als Deployment File ausgegeben werden.

kubectl create/apply -f {deployment-file}
: Mit _kubectl create/apply_ können Ressourcen auf die Plattform deployed werden. Der unterschied der beiden Befehle ist, dass bei _create_ die Ressource vorher nicht bereits bestehen darf, die deployed werden soll, sonst wird ein Fehler gemeldet. Bei _apply_ wird eine bestehende Ressource überschrieben.

kubectl describe {ressource} \[id]
: Mit _kubectl describe_ können detaillierte Information zu allen laufenden Ressourcen auf der Plattform abgefragt werden.

kubectl edit {ressource} {id}
: Mit _kubectl edit_ kann eine Instanz einer Ressource live auf der Plattform bearbeitet werden.

kubectl delete {ressource} {id}
: Mit _kubectl _ kann eine Instanz einer Ressource von der Plattform gelöscht werden.

kubectl scale {ressource} {id}
: Mit _kubectl scale_ können bestimmte Arten von Ressourcen (z.B. Deployments oder StatefulSets) manuell hoch oder herunter skaliert werden.

kubectl logs {pod-id} [-c {container}]
: Mit _kubectl logs_ kann man die Ausgaben eines Containers (eines Pods), die auf Standard Out geschrieben wurden, von der Plattform abfragen. Dies ist nur für Pods möglich!

kubectl exec -it {pod-id} [-c {container}] {command}
: Mit _kubectl exec_ kann man sich gleich wie bei _docker exec_ auf den laufenden Container in dem Pod verbinden. Dies ist nur für Pods möglich!

kubectl cp {source} {destination}
: Mit _kubectl cp_ können Dateien aus einem Container heruntergeladen oder Dateien in einen Container hochgeladen werden. Das Format, wenn die Quelle oder das Ziel ein Container ist, sieht folgendermaßen aus: [{namespace}/]{pod-id}:/path/to/file

Aufgrund unserer Entwicklertätigkeit und dem Hang zur Optimierung sind für folgende Befehle bereits Aliase auf der Shell definiert.

<a id="kubernetes-aliase" />

Befehl                | Alias
----------------------|------
kubectl               | kctl
kubectl get           | kg
kubectl get pods      | kgp
kubectl describe      | kd
kubectl describe pods | kdp
kubectl logs          | kl
kubectl exec -it      | kex 

[Zur Agenda](#agenda)

### Kubernetes Deployment Templates

Alle Kubernetes Deployment Files sind im YAML Format.

#### Service

<details><summary>Click to open</summary>
```yaml
kind: Service
apiVersion: v1
metadata:
  name: {NAME}
spec:
  selector:
    app: {NAME}
  ports:
  - name: {NAME}
    port: {KUBERNETES-INTERNAL-PORT}
```
</details>

#### Deployment

<details><summary>Click to open</summary>
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {NAME}
  labels:
    app: {NAME}
spec:
  replicas: 3
  revisionHistoryLimit: 3
  selector:
    matchLabels:
      app: {NAME}
  strategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: {NAME}
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        imagePullPolicy: Always
        ports:
        - containerPort: 80
```
</details>

#### StatefulSet

<details><summary>Click to open</summary>
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {NAME}
spec:
  selector:
    matchLabels:
      app: {NAME} # has to match .spec.template.metadata.labels
  serviceName: "{NAME}"
  replicas: 3 # by default is 1
  template:
    metadata:
      labels:
        app: {NAME} # has to match .spec.selector.matchLabels
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: {NAME}
        image: k8s.gcr.io/nginx-slim:0.8
        ports:
        - containerPort: 80
          name: web
        volumeMounts:
        - name: www
          mountPath: /usr/share/nginx/html
  volumeClaimTemplates:
  - metadata:
      name: www
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```
</details>

#### Ingress

<details><summary>Click to open</summary>
```yaml
kind: Ingress
apiVersion: extensions/v1beta1
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
  name: {NAME}
spec:
  rules:
  - host: {USERNAME}.k.vision-spirit21.com
    http:
      paths:
      - backend:
          serviceName: {NAME}
          servicePort: {SERVICE-PORT}
        path: {SUB-PATH}
  tls:
  - hosts:
    - {USERNAME}.k.vision-spirit21.com
    secretName: {USERNAME}-crt
```
</details>

#### Config Map / Secret

##### ConfigMap

<details><summary>Click to open</summary>
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {NAME}
data:
  file.conf: |-
    <Multiline file content>
  key: value  
```
</details>

##### Secret

<details><summary>Click to open</summary>
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: {NAME}
data:
  key: BASE64(<VALUE>)
```
</details>

#### Jobs

<details><summary>Click to open</summary>
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: {NAME}
spec:
  template:
    spec:
      containers:
      - name: pi
        image: perl
        command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
      restartPolicy: Never
  backoffLimit: 10
```
</details>

#### Persistent Volume Claim

<details><summary>Click to open</summary>
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {NAME}
spec:
  accessModes: [ "ReadWriteOnce" ]
  resources:
    requests:
      storage: 1Gi
```
</details>

[Zur Agenda](#agenda)

### Kubernetes Aufgaben

In diesem Teil geht es darum, Kubernetes Deployment Files zu erstellen, damit diese die Beispiel Anwendung korrekt auf einem AWS Kubernetes Cluster deployen. Die Übungen sehen vor, dass die jeweiligen Beispiele aus dem vorherigen Kapitel herangezogen werden und mit den angegebenen Daten geändert bzw. erweitert werden.

Nach jedem Schritt sollte überprüft werden, ob das gerade ausgeführt Deployment auch ordnungsgemäß durchgeführt wurde und ob alle Pods korrekt hochgefahren wurden (siehe [Kubernetes Befehle](#kubernetes-befehle)). 

#### Aufgaben

Alle [Toplevel Elemente](#kubernetes-ressourcen) werden in Kubernetes als Ressourcen bezeichnet. Wenn mehrere Ressourcen mit einem File deployed werden sollen, müssen die Ressourcen durch drei Bindestriche (-\--) in einer neuen Zeile voneinander getrennt werden. **Wichtig beim Dateiformat YAML ist, dass die Einrückungen essentiell wichtig sind!**

1. Für die erste Übung haben wir ein fertiges Deployment File vorbereitet, das eine NoSQL Datenbank auf Kubernetes mit 3 Instanzen deployed. Dazu wird noch eine Konfiguration für die Datenbanken erstellt, die zentral als Kubernetes Ressource verfügbar ist und von allen MongoDB Pods verwendet wird. Zusätzlich wird noch ein Service erstellt, der die Erreichbarkeit der Datenbank von anderen Pods aus ermöglicht. Verwendet wird hier auch eine automatische Provisionierung von Persistent Volumes mit einer Größe von 1GB. 
<details><summary>Click to open</summary>
    **Setup**
    Bitte navigieren Sie dazu auf der Shell in den Ordner _~/docker-workshop/deployment-files_. Hier finden Sie für diesen Workshop bereits vorbereitete Deployment Files. Bitte navigieren Sie in den Ordner für die _Aufgabe1_. für die erstellen Sie eine Datei auf dem Client mit dem Namen _mongo.yaml_ und fügen folgenden Text darin ein. Bitte ändern Sie den Namen des MongoDB Images entsprechend dem, was Sie vorher in die Remote Registry [gepushed haben](#beispiel-applikation) (StatefulSet: spec.template.spec.containers[0].image).  
  
    **Deployment**
  
  * Deployen Sie nun die Datei mit `kubectl apply -f mongo.yaml` (siehe [Befehle](#kubernetes-befehle)) oder `kctl apply -f mongo.yaml` (siehe [Aliase](#kubernetes-aliase)).
  * Der Output auf der Konsole sollte folgendermaßen aussehen
  
    ```
    $ kctl apply -f mongo.yaml
    configmap "mongo" created
    service "mongo" created
    statefulset "mongo" created
    ```
  
    **Überprüfung**
    Nun wollen wir uns ansehen, was wir erzeugt haben. Das Ergebnis sollte sein, dass 3 Pods nacheinander auf dem Kubernetes Cluster gestartet wurden.
  
  * Ausgeben der aktuell ausgeführten Pods mit `kubectl get pods` oder `kgp`. Warten Sie, bis alle 3 Pods erfolgreich gestartet sind.
  * Wir können auch die Log-Ausgaben der MongoDB ansehen, um zu verifizieren, dass die Datenbank korrekt gestartet ist. Führen Sie dazu einen der folgenden Befehle aus: `kubectl logs mongo-0` oder `kl mongo-0`.
</details>

2. Im zweiten Schritt müssen wir die Datenbank einmalig so konfigurieren, dass die einzelnen Member voneinander wissen, damit sie einen Cluster bilden können. Dafür verwenden wir einen Kubernetes Job.
<details><summary>Click to open</summary>
    Bitte navigieren Sie dazu in den Ordner _Aufgabe2_. Dort sollte sie eine Datei mit dem Namen _mongo-config.yaml_ finden. Bitte ändern Sie den Namen des MongoDB Images entsprechend dem, was Sie vorher in die Remote Registry [gepushed haben](#beispiel-applikation) (Job: spec.template.spec.containers[0].image).
  
    **Deployment**
  
  * Deployen Sie nun die Datei mit `kubectl apply -f mongo-config.yaml` (siehe [Befehle](#kubernetes-befehle)) oder `kctl apply -f mongo-config.yaml` (siehe [Aliase](#kubernetes-aliase)).
  * Der Output auf der Konsole sollte folgendermaßen aussehen
  
    ```
    $ kctl apply -f mongo-config.yaml
    job "mongo-config" created
    ```
  
    **Überprüfung**
    Nun wollen wir uns ansehen, was sich durch das Job Deployment geändert hat. Das Ergebnis sollte sein, dass der Job die Datenbank Instanzen erfolgreich verbunden hat.
  
  * Ausgeben der aktuell ausgeführten Pods mit `kubectl get pods` oder `kgp`. Da der Job allerdings ein One-Time-Job ist, ist der Pod, der vom Job erstellt wurde hier nicht mehr zu sehen. Mit dem Befehl `kubectl get pods -a` oder `kgp -a` können alle Pods angezeigt werden, auch die Pods, die durch einen Job zwar noch "bestehen" aber schon beendet sind. Normalerweise werden Pods, die z.B. von einem ReplicaSet erstellt wurden und nicht mehr benötigt werden gelöscht und nicht gestoppt!
  * Wir können auch die Log-Ausgaben der MongoDB ansehen, um zu verifizieren, dass die Datenbanken korrekt konfiguriert wurden. Führen Sie dazu einen der folgenden Befehle aus: `kubectl logs mongo-0` oder `kl mongo-0`.
  * Um zu sehen, wie die Konfiguration des Datenbank Clusters aussieht, können wir in einem der Datenbank Pods einen Befehl absetzen, der uns die gewünschten Daten ausgibt.
  
    ```
    kubectl exec -it mongo-0 mongo
    > rs.status()
    ``` 
    
    oder
    
    ```
    kex mongo-0 mongo
    > rs.status()
    ```
  * Der Output sollte 3 Member enthalten, entsprechend der Konfiguration von oben.
</details>

3. Im dritten Schritt wollen wir das Backend deployen. Hiermit wollen wir die Kommunikation zwischen zwei Anwendungen (in diesem Fall die Datenbank und ein Backend) innerhalb von Kubernetes testen. Für die Kommunikation ist wichtig, wie das Backend mitbekommt, wo die Datenbank im Cluster zu finden ist. Dies wird über einen Kubernetes Service realisiert, d.h. der Name des Services für die Mongo ist gleichzeitig der DNS Name, unter dem der Service und damit auch die Datenbank innerhalb des Kubernetes Clusters angesprochen werden kann. 
<details><summary>Click to open</summary>
   **Werte Ersetzen**
   Bitte navigieren Sie in den Ordner _Aufgabe3_. In diesem Ordner finden Sie die zwei Dateien _api.final.yaml_ und _api.yaml_. Die Datei _api.yaml_ enthält Platzhalter die passend ersetzt werden müssen. Ersetzen sie dazu bitte die folgenden Werte:
    
   * Den Name der ConfigMap mit dem Namen der ConfigMap welche das Deployment mittels _configMapKeyRef_ referenziert.
   * Den Selector des Services, sodass er auf die Label des Deployments passt. Und damit als LoadBalacer für dieses Deployment dient.
   * Den Port des Services, sodass er den richtigen Port der Pods anspricht.
   * Den ServiceName welcher im Ingress definiert ist, damit das Ingress auf den gerade erstellten Service zeigt.
   * Alle Vorkommnisse der _{USERNAME}_ Platzhalters
        
    In der Datei _api.final.yaml_ finden Sie zur Hilfe ein bereits ersetztes File.
     
    **Deployment**
    Bitte wenden Sie die eben ersetzte Datei wie oben schon beschrieben mit `kubectl apply -f api.yaml` an. Der Consolen Output sollte folgendermaßen aussehen.
  
    ```
    $ kctl apply -f api.final.yaml
    configmap "api" created
    service "api" created
    deployment "api" created
    ingress "api" created
    ``` 
     
    **Überprüfen**
    Jetzt wollen wir überprüfen, ob das Backend auch mit der Datenbank sprechen kann. Dazu lassen wir uns die Logs von einem der Backend Pods ausgeben. Dazu verwenden wir den Befehl `kubectl logs api-xxxxx-xxxxx` oder `kl api-xxxxx-xxxxx`.
    Außerdem können wir über die URL, welche wir im Ingress definiert haben (z.B. `mhaug.k.vision-spirit21.com`) und der Kontext Route `/api/` also zusammengesetzt zum Beispiel `https://mhaug.k.vision-spirit21.com/api/` das Backend aufrufen. 
    Das SSL Zertifikat ist leider ungültig. Dies kommt davon, dass wir für diesen Workshop das Staging Environment von [Let's Encrypt](https://letsencrypt.org/) verwenden. Dadurch können wir es allen Workshop Teilnehmern ermöglichen, automatisiert ein SSL/TLS Zertifikat zu erhalten. 
    **Sie können die Security Warnung deshalb überspringen** und werden dann ein Seite mit dem Titel `Whitelabel Error Page` und im Beschreibungstext unten `type=Unauthorized, status=401` erhalten. Diese Seite wird ausgeliefert, weil wir sie ohne gültige Zugangsdaten aufgerufen haben. Wenn Sie diese Seite sehen, hat das Deployment funktioniert.
</details>

4. Im vierten Schritt werden wir die UI deployen. Hiermit wollen wir die zwischen Anwendungen außerhalb (Browser) und innerhalb (Backend, Webserver) des Kubernetes Clusters testen. Dazu wird ein sogenannter _Ingress_ verwendet, der deklarativ das Routing von eingehendem HTTP(S) Traffic übernimmt. Einfach ausgedrückt wird mit einer Kubernetes Ressource konfiguriert, unter welchem Hostnamen und welchem Kontext-Pfad welcher Service und Port angesprochen werden soll.
<details><summary>Click to open</summary>
    **Deployment**
     Bitte navigieren Sie dazu in den Ordner _Aufgabe4_. Ersetzen Sie auch hier alle Vorkommnisse von `{USERNAME}` und deployen sie die Datei im Anschluss mit den bekannten Befehlen.
     
     **Überprüfung**
     Rufen Sie zur Überprüfen des Deployments zuerst die Übersicht aller Pods auf (`kubectl get pods` oder `kgp`). Nachdem Sie gesehen haben, dass mindestens einer der UI Pods gestartet wurde, rufen sie bitte erneut ihre Domain (`{USERNAME}.k.vision-spirit21.com`) ohne Kontext Pfad auf. Sie werden nun auf die Domain `https://sso.fstorz.de` weitergeleitet und hier sehen sie die Meldung _Invalid parameter: redirect_uri_. Wenn sie diese Seite erreicht haben, wenden Sie sich an einen Vortragenden, welcher sie hier manuell freischalten muss.
     Nachdem dies geschehen ist, können Sie die Seite erneut aufrufen und sich registrieren. Hier können sie beliebige Daten eintragen. Nach der erfolgreichen Registrierung und anschließendem Login, werden sie wieder zurück auf Ihre UI geleitet.
     Hier können Sie jetzt den simplen "Hero Service" bedienen. Sie sehen in der Mitte oben den Namen, mit welchem Sie sich eben registriert haben. Sie können in diesem Service Einträge in eine Datenbank schreiben, diese verändern, ansehen und auch wieder löschen.
</details>

5. Resourcen Beschränkung einrichten. Aktuell kann jeder Pod so viele Ressourcen auf seiner Node verbrauchen, wie er möchte. Um die Reproduzierbarkeit von Deployments und die Stabilität des Clusters sicherzustellen, können Limits für den Verbrauch von Ressourcen festgelegt werden.
<details><summary>Click to open</summary>
     Bitte navigieren Sie in den Ordner _Aufgabe5_6_. Hier fügen Sie die folgenden Code Snippets in die bereits erstellten Deployments und StatefulSets ein.
    
    **Mongo**
     Einfügen unter _spec.template.spec.containers[0].resources_
    
     ```
            resources:
              requests:
                memory: 256M
                cpu: 0.1
              limits:
                memory: 1024M
                cpu: 1
     ```
     
    **API**
     Einfügen unter _spec.template.spec.containers[0].resources_
    
     ```
            resources:
              requests:
                memory: 512M
                cpu: 0.1
              limits:
                memory: 2048M
                cpu: 1.5
     ```
     
    **UI**
     Einfügen unter _spec.template.spec.containers[0].resources_
    
     ```
            resources:
              requests:
                memory: 100M
                cpu: 0.1
              limits:
                memory: 1024M
                cpu: 1
     ```
     
     **Deployment und Tests**
     Bitte deployen Sie jetzt alle geänderten Dateien erneut. Die Ausgabe sollte bei dem MongoDB Deployment zum Beispiel wie folgt aussehen:
     ```
     $ kctl apply -f mongo.yaml
     configmap "mongo" unchanged
     service "mongo" unchanged
     statefulset "mongo" configured
     ```
     Wie Sie sehen, erkennt Kubernetes automatisch den geänderten Teil der Datei und wendet nur diesen an.
     
     Wenn Sie sich nun die Pods ausgeben lassen, werden sie erkennen, dass Kubernetes alle betroffenen Pods neu startet, jedoch nicht geleichzeitig, sondern sequenziell. Dadurch entstehen trotz des Updates keine Ausfälle der Anwendung.
</details>

6. Self-Healing einrichten. Um es Kubernetes zu ermöglichen, fehlerhafte Pods zu erkennen und sie bei Bedarf neuzustarten, muss für den Pod eine sogenannte _LivenessProb_ definiert werden. Mit der _LivenessProbe_ wird zyklisch geprüft, ob die Anwendung noch "korrekt" funktioniert. Die Prüfung kann z.B. ein Aufruf einer Netzwerkschnittstelle oder ein Kommando innerhalb des Containers sein. Wenn dieser Befehl eine bestimmte Anzahl hintereinander nicht erfolgreich ausgeführt werden kann, wird der Pod neugestartet. Im Gegesatz dazu gibt es auch noch die ReadinessProbe, welche beim Fehlschlag den Pod nur aus dem LoadBalancing des Services nimmt, ihn aber nicht neustartet. Diese Funktionalität kann zum Beispiel für langlaufende Updates verwendet werden, welche den Pod für den Service nicht ansprechbar macht. In der Praxis sind diese beiden Befehle jedoch meistens gleich.
<details><summary>Click to open</summary>    
    **UI**
    Beginnen wir mit der einfachsten Liveness- und ReadinessProbe. Fügen Sie unter `spec.template.spec.containers[0]` den folgenden Code Ausschnitt in die bereits bestehenden Deployment Files ein.
    
     ```
            livenessProbe:
              httpGet:
                path: /index.html
                port: 80
              initialDelaySeconds: 10
              periodSeconds: 10
            readinessProbe:
              httpGet:
                path: /index.html
                port: 80
              initialDelaySeconds: 10
              periodSeconds: 10
     ```
     
    **API**
    Hierbei wird der Pfad `/mgmt/health` des Pods aufgerufen. Wenn dieser auf diesem Pfad keine valide Antwort erhalten wird ist die Anwendung auch nicht mehr in der Lage andere Anfragen zu bearbeiten und ist damit nicht mehr  'gesund' und muss neu gestartet werden. Um diese Prüfung einzubauen, fügen Sie bitte den folgenden Ausschnitt in `spec.template.spec.containers[0]` ein.
    
     ```
            livenessProbe:
              httpGet:
                path: /mgmt/health
                port: 8080
              initialDelaySeconds: 15
              periodSeconds: 10
            readinessProbe:
              httpGet:
                path: /mgmt/health
                port: 8080
              initialDelaySeconds: 15
              periodSeconds: 10
     ```
     
     **Mongo**
     Die MongoDB stellt keine HTTP Ressource bereit. Aus diesem Grund muss hier eine andere Prüfung verwendet werden. Zu Auswahl steht eine TCP Prüfung und eine Command Prüfung. Mit der TCP Prüfung kann auf das Vorhandensein eines bestimmten Ports geprüft werden. Da dies jedoch nicht viel über die tatsächliche Verfügbarkeit aussagt, verwenden wir in diesem Beispiel die Command Prüfung. Dieser Check überprüft ob der aktuelle Status des Pods innerhalb eines definierten Zustandes ist. Für diese Prüfung fügen Sie bitte dieses Snippet unter `spec.template.spec.containers[0]` ein.
     
     ```
            # states [1-2]/[1-5] are corresponding to https://docs.mongodb.com/manual/reference/replica-states/
            livenessProbe:
              exec:
                command:
                - bash
                - -c
                - "mongo --eval 'rs.status()'| grep myState | grep -q ' [1-5]'"
              initialDelaySeconds: 120
              periodSeconds: 5
     ``` 
     **Deployment und Tests**
     Bitte deployen Sie jetzt alle geänderten Dateien erneut. Siehe Aufgabe 5.
</details>

#### Optional

Deployment eines zentralen Logging Systems. Dieses System bestehet aus zwei Komponenten. Der eine Teil sind verteilte Log Shipper. Diese lesen die Log Dateien auf den verschiedenen Pods aus und senden sie an die zweite Komponente. Diese zweite Komponente ist eine Instanz der Logstash Anwendung. Diese konsolidiert die Logfiles, nimmt wenn nötig transformationen vor und schreibt sie in einen persistenten Speicher.

1. Deployment logstash
<details><summary>Click to open</summary>
    Im Ordner _OptionalLogging_ finden Sie die Deployment Files für diese Infrastruktur. Fügen sie den unten dargestellten securityContext an der Stelle `spec.template.spec.securityContext` in die Datei _logstash.yaml_ ein. Dadurch wird der Prozess innerhalb des Pods nicht mehr als `root` Benutzer ausgeführt. Dies ist eine Maßnahme die in manchen Kundenumgebungen gefordert ist um die Sicherheit zu erhöhen. Außerdem wird mit dem `fsGroup` Key eine Benutzergruppe erzeug welche automatisch auf die eingebundenen Volumes berechtigt ist.
    
     ```
          securityContext:
            fsGroup: 1000
            runAsUser: 1000
            runAsNonRoot: true
     ```
</details>

2. API Deployment.
<details><summary>Click to open</summary>
    Bitte aktivieren Sie erst das Logging in ein File mit der folgenden Konfiguration unter `spec.template.spec.containers[0].env`
    
    ```
            - name: LOGGING_FILE
              value: "/var/log/app/api.log"
    ```
    
    Für den Log Shipper müssen wir dem API Pod einen weiteren Container hinzufügen. Fügen sie dazu bitte das folgende Snippet unter `spec.template.spec.containers[1]` ein. 
    ```
          - name: filebeat
            image: reg.k.vision-spirit21.com/filebeat:6.1.2
            imagePullPolicy: Always
            resources:
              limits:
                memory: 200Mi
                cpu: 200m
              requests:
                cpu: 100m
                memory: 100Mi
            volumeMounts:
            - name: etc
              mountPath: /usr/share/filebeat/filebeat.yml
              subPath: filebeat.yml
    ```
    
    Um die Dateien zwischen den beiden Containern des Pods zu teilen müssen wir ein geteiltes Volume einrichten. Dieses wird vom Typ `emptyDir` sein und initial nichts enthalten. Dieses Volume hat die selbe Lebensdauer wie der Pod und dient nur dem Teilen von Dateien. Definieren sie dazu unter `spec.template.spec.volumes` das Volume mit:
    ```
          - name: logs
            emptyDir: {}
    ```
    
    und mounten Sie es in beide Container an die Stelle `spec.template.spec.container[*].volumeMounts` mit:
    ```
            volumeMounts:
            - name: logs
              mountPath: /var/log/app
    ```
    Bitte wenden sie jetzt das neue File an.
    
    **Test**
    Bitte öffnen sie eine bash innerhalb des Logstash containers mit `kex logstash-645c8ccdbc-lxqcv bash` oder `kubectl exec -it logstash-645c8ccdbc-lxqcv bash`. Navigieren Sie in das Verzeichnis `/var/log/app/`. Dort sehen Sie das konsolidierte Logfile beider API Instanzen.
</details>

[Zur Agenda](#agenda)