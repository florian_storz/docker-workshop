# Bilder

Bild           | H�he (px)
---------------|----------
docker.png     | 200
kubernetes.png | 200

# Markdown

The markdown implementation is from <https://github.com/russross/blackfriday>.<br>
Styling ist from <https://github.com/sindresorhus/github-markdown-css>

# Local Running

    docker build -t docu .
    docker run --rm -p 8080:8080 docu
    
Open a browser an open the page 'localhost:8080'

# Manual Installation

## Install GO Dep 

Download Go Dep from <https://github.com/golang/dep/releases>. Afterwards copy the file to the GO Install directory /bin folder and rename to dep.exe (GO >= 1.9).

## Init Project

open cmd and go to {project base dir}/src/main and execute following command

    dep ensure
    
The dep tool will download all required dependencies.

## Build within Docker for Linux

docker run -it --rm -v path\to\workshop-docu:/go -p 8080:8080 golang
> go install main

The compiled server is located under bin/main