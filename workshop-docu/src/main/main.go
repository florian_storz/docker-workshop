package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/microcosm-cc/bluemonday"
	"gopkg.in/russross/blackfriday.v2"
)

var markdownDir string
var assetDir string

// Default Request Handler
func defaultHandler(w http.ResponseWriter, r *http.Request) {
	if html, err := ioutil.ReadFile(markdownDir + "/base.html"); err != nil {
		log.Fatalf("Cannot read html err[%s]", err.Error())
		http.Error(w, "Cannot read html", 500)
	} else if readme, err := ioutil.ReadFile(markdownDir + "/info.md"); err != nil {
		log.Fatalf("Cannot read html err[%s]", err.Error())
		http.Error(w, "Cannot read docu", 500)
	} else {
		unsafe := blackfriday.Run(readme, blackfriday.WithExtensions(blackfriday.CommonExtensions|blackfriday.AutoHeadingIDs|blackfriday.HardLineBreak|blackfriday.HeadingIDs))
		body := bluemonday.UGCPolicy().SanitizeBytes(unsafe)

		fmt.Fprintf(w, string(html), string(body))
	}
}

func assetHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if content, err := ioutil.ReadFile(assetDir + "/" + vars["name"]); err != nil {
		log.Fatalf("Cannot read asset: %s", "assets/"+vars["name"])
		http.Error(w, "Cannot read file", 500)
	} else {
		fmt.Fprint(w, string(content))
	}
}

func main() {
	markdownDir = os.Getenv("MARKDOWN_DIR")
	if len(markdownDir) == 0 {
		markdownDir = "markdown"
	}
	log.Printf("Markdown dir: %s", markdownDir)

	assetDir = os.Getenv("ASSET_DIR")
	if len(assetDir) == 0 {
		assetDir = "assets"
	}
	log.Printf("Asset dir: %s", assetDir)

	r := mux.NewRouter()

	r.HandleFunc("/", defaultHandler).Methods("GET")
	r.HandleFunc(`/assets/{name:.+}`, assetHandler).Methods("GET")

	log.Println("Start Server on Port 8080")

	log.Fatal(http.ListenAndServe(":8080", r))

}
