import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';

import { KeycloakService } from './keycloak.service';
import { Observable } from 'rxjs/Rx';

/**
 * This provides a interceptor over the ng2 HttpClient class that insures tokens are refreshed on each request.
 */
 @Injectable()
 export class TokenInterceptor implements HttpInterceptor {
   constructor(private _keycloakService: KeycloakService) {}

   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
     const tokenPromise: Promise<string> = this._keycloakService.getToken();
     const tokenObservable: Observable<string> = Observable.fromPromise(tokenPromise);

     return tokenObservable
         .map(token => {
             request = request.clone({
               setHeaders: {
                 Authorization: `Bearer ${token}`
               }
             });
             return request;
         })
         .concatMap(request => next.handle(request));
   }
 }
