import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { KeycloakService } from './keycloak.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Heroes Of The World';
  profile: User;

  constructor(private keycloakService: KeycloakService) {
  }

  public ngOnInit(): void {
      this.profile = this.keycloakService.getUser();
  }

  public logout() {
      this.keycloakService.logout();
  }
}
