export class User {
  firstName: string;
  username: string;
  lastName: string;
  email: string;
}
