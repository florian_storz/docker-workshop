export const environment = {
  production: true,
  KEYCLOAK_URL: 'https://sso.fstorz.de/auth',
  KEYCLOAK_REALM: 'Demo',
  KEYCLOAK_CLIENTID: 'frontend',
  BACKEND_URL: '/api'
};
