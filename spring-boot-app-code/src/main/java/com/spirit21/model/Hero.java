package com.spirit21.model;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@Document
@ToString
public class Hero {

	@JsonProperty("id")
	private String _id;
	private String name;
	
	public Hero(String name) {
		this.name = name;
	}
	
}
