package com.spirit21.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.spirit21.model.Hero;

@Repository
public interface HeroRepository extends MongoRepository<Hero, String> {
}
