package com.spirit21.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.spirit21.model.Hero;

@Service
public class HeroService {

	@Autowired
	private HeroRepository heroRepository;

	@Autowired
	private MongoOperations mongoOperation;

	public Collection<Hero> getHeroes() {
		return heroRepository.findAll();
	}

	public Optional<Hero> getHero(String id) {
		return Optional.ofNullable(heroRepository.findOne(id));
	}

	public void updateHero(Hero hero) {
		heroRepository.save(hero);
	}

	public Hero addHero(Hero hero) {
		return heroRepository.save(hero);
	}

	public void removeHero(Hero hero) {
		heroRepository.delete(hero);
	}

	public Collection<Hero> searchHero(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").regex(name, "i"));

		return mongoOperation.find(query, Hero.class);
	}
}
