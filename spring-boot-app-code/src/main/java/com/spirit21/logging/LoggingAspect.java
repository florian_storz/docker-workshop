package com.spirit21.logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.EntryMessage;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    @Around("@within(com.spirit21.logging.Loggable) || @annotation(com.spirit21.logging.Loggable)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        Logger log = LogManager.getLogger(joinPoint.getTarget().getClass());
        String name = joinPoint.getSignature().getName();

        EntryMessage message = log.traceEntry("{}({})", name, joinPoint.getArgs());

        Object result = null;
        Throwable exception = null;
        try {
            result = joinPoint.proceed(joinPoint.getArgs());
        } catch (Throwable e) {
            exception = e;
        }

        if (exception == null) {
            return log.traceExit(message, result);
        } else {
            throw log.throwing(Level.DEBUG, exception);
        }

    }
}
