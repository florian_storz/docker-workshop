package com.spirit21.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.spirit21.logging.Loggable;
import com.spirit21.model.ClientErrorException;
import com.spirit21.model.Hero;
import com.spirit21.model.NotFoundException;
import com.spirit21.service.HeroService;

import lombok.extern.log4j.Log4j2;

@CrossOrigin
@RestController
@RequestMapping("/api/heroes")
@Log4j2
@Loggable
public class WebController {

	@Autowired
	private HeroService service;

	@GetMapping
	public Collection<Hero> getHeroes(@RequestParam(required = false) String name) {
	    log.info("Requested Heroes with param [{}]", name);

	    Collection<Hero> heroes;
	    
	    if (StringUtils.isEmpty(name)) {
			heroes = service.getHeroes();
		} else {
		    heroes = service.searchHero(name);
		}
        
		return heroes;
	}
	
	@PostMapping
	public Hero createHero(@RequestBody Hero hero, UriComponentsBuilder b) {
		return service.addHero(hero);
	}

	@GetMapping("/{id}")
	public Hero getHeroById(@PathVariable String id) throws NotFoundException, ClientErrorException {
		return service.getHero(id).orElseThrow(() -> new NotFoundException());
	}

	@PutMapping("/{id}")
	public void updateHero(@PathVariable String id, @RequestBody Hero hero) throws NotFoundException, ClientErrorException {
		if (!id.equals(hero.get_id())) {
			throw new ClientErrorException();
		}

		getHeroById(id);

		service.updateHero(hero);
	}

	@DeleteMapping("/{id}")
	public void deleteHero(@PathVariable String id) throws NotFoundException, ClientErrorException {
		Hero hero = getHeroById(id);

		service.removeHero(hero);
	}
}
