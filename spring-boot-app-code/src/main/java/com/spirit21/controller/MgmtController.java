package com.spirit21.controller;

import org.springframework.boot.actuate.health.Health;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mgmt/health")
public class MgmtController {

    @GetMapping
    public Health health() {
        return Health.up().build();
    }
}
