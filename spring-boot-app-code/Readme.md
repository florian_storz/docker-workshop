# Spring Boot API

## Security

Diese Applikation is gesichert mit dem OpenID-Connect Protokoll. 

## Lokales Testen

Um die App lokal laufen zu lassen, müssen folgende Bendingungen erfüllt sein.

* Eine MongoDB muss gestartet sein und auf dem Port 27017 lauschen
	
```
docker create network app
docker run -d -p 27017:27017 --name mongo --network app mongo
```

* Run the API
      
    docker run -d -p 8080:8080 --name api --network app -e spring.data.mongodb.host=mongo florian-pc:15800/spring-boot-app

* Ein Keykload server muss erreichbar sein

```

```

* Ein Nexus 3 Repository Manager muss verfügbar sein und der 

```
docker volume create --name nexus-data
docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3
docker run -d -p 8444:8444 --name nexus-nginx --network nexus -v C:\Entwicklung\git\common\conf.d:/etc/nginx/conf.d -v C:\Entwicklung\git\common\ssl:/etc/nginx/ssl nginx
```